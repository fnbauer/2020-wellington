# Copyright 2019 James Polley
# Copyright 2019 Matthew B. Gray
# Copyright 2019 Steven C Hartley
# Copyright 2019 AJ Esler
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# https://docs.gitlab.com/ee/ci/quick_start/
image: docker:stable

services:
  - docker:dind

stages:
  - build
  - test
  - publish

# For more naming options, see https://docs.gitlab.com/ee/ci/variables/
variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  CONTAINER_TEST_IMAGE: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-test
  GITLAB_CI_RUNNING: "true"

before_script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com

docker_build:
  stage: build
  script:
    - docker build --target=development --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE

rspec_tests:
  stage: test
  script:
    - docker run -d --name="test-database" --hostname "postgres" postgres:latest
    - docker run --network "container:test-database" $CONTAINER_TEST_IMAGE bundle exec rake db:create db:structure:load spec

code_style:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE bundle exec rubocop
    - docker run $CONTAINER_TEST_IMAGE ./node_modules/eslint/bin/eslint.js app/javascript

licence_check:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE bundle exec rake test:branch:copyright

security_analysis:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE bundle update brakeman --quiet
    - docker run $CONTAINER_TEST_IMAGE bundle exec brakeman --run-all-checks --no-pager
    - docker run $CONTAINER_TEST_IMAGE bundle exec bundler-audit check --update
    - docker run $CONTAINER_TEST_IMAGE bundle exec ruby-audit check
    - docker run $CONTAINER_TEST_IMAGE yarn audit
    - docker run $CONTAINER_TEST_IMAGE yarn check --integrity

release_image:
  stage: publish
  variables:
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
    - docker tag $CONTAINER_RELEASE_IMAGE $CI_REGISTRY_IMAGE:stable
    - docker push $CI_REGISTRY_IMAGE:stable
  only:
    - tags

latest_image:
  stage: publish
  variables:
    CONTAINER_RELEASE_IMAGE: ${CI_REGISTRY_IMAGE}:latest
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - master
